import axios from 'axios'
import {Message} from 'element-ui'
import cookie from 'js-cookie'
// 创建axios实例
const service = axios.create({
  baseURL: 'http://localhost:9001', // api的base_url
  timeout: 20000 // 请求超时时间
})

//http request拦截器
service.interceptors.request.use(
  config => {
    if(cookie.get('guli_token')){
      //把获取到的cookie值放到header中
      config.headers['token'] = cookie.get('guli_token');
    }
    return config
  },
  err =>{
    return Promise.reject(err)
  }
)

//http response 拦截器
service.interceptors.response.use(
  res =>{
    if(res.data.code == 28004){
      console.log("res.data.code.sultCode is 28004")
      window.location.href='/login'
      return
    }else {
      if(res.data.code != 20000){
        if(res.data.code != 25000){
          Message({
            message:res.data.message || 'error',
            type:'error',
            duration:5 * 1000
          })
        }
      }else{
        return res
      }
    }
    
    
  }
)



export default service