import request from '@/utils/request'

export default {

     //分页评论查询
     getCommentList(page,limit,courseId){
        return request({
            url:`/eduservice/comment/pageList/${courseId}/${page}/${limit}`,
            method:'post'
        })
    },

    //保存评论
    save(eduComment){
      return request({
        url:`/eduservice/comment/save`,
        method:'post',
        data:eduComment
      })
    }

}