import request from '@/utils/request'

export default {

     //分页课程查询
     getCourseList(page,limit,courseFrountVo){
        return request({
            url:`/eduservice/coursefront/getCourseFrontList/${page}/${limit}`,
            method:'post',
            data:courseFrountVo
        })
    },

    //获取所有课程分类
    getAllSubjcet(){
      return request({
        url:`/eduservice/subject/getAllSubject`,
        method:'get'
      })
    },
    //获取课程详情
    getCourseInfo(id){
      return request({
        url:`/eduservice/coursefront/getCourseFrontInfo/${id}`,
        method:'get'
      })
    }


}