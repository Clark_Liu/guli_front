import request from '@/utils/request'

export default {

    //查询前两条banner图片
    getBannerList(){
        return request({
            url:'/educms/bannerfront/getAllBanner',
            method:'get'

        })
    }


}