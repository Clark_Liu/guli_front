import request from '@/utils/request'

export default {


    wxLogin(){
        return request({
            url:`/api/ucenter/wx/login`,
            method:'get',
        })
    },


    //提交登录
    submitLoginUser(userInfo){
        return request({
            url:`/educneter/member/login`,
            method:'post',
            data:userInfo
        })
    },
    //根据token获取用户信息
    getLoginUserInfo(){
        return request({
            url:`/educneter/member/getMemberInfo`,
            method:'get'
        })
    }

}